# Browser-Based IFR Simulation

[![CircleCI](https://circleci.com/bb/eburlingame/isim.svg?style=svg)](https://circleci.com/bb/eburlingame/isim)

## Objective

The goal of this project is to design and develop an in-browser flight simulator aimed at pilots learning to fly on instruments. This project will center around an accurate instrument panel based on a very common training aircraft, the Cessna 172. A pilot will be able to fly the simulated aircraft with a mouse or joystick, and also interact with the instruments on the panel (radios, VORS, engine controls, etc.). 

## Implementation Overview

I will use JSBSim (http://jsbsim.sourceforge.net/) as an underlying flight simulation physics engine. This is a well-established flight dynamics model that is used in the open-source desktop flight simulator FlightGear (http://home.flightgear.org/). It is a C++ library, which will be ported to JavaScript for use in the browser using Emscripten (http://kripken.github.io/emscripten-site/). Emscripten is an “LLVM-based project that compiles C and C++ into highly-optimizable JavaScript in asm.js format.” 

JSBSim has been transpiled with Emscripten in other projects, including this project: https://github.com/csbrandt/JSBSim.js. This GitHub project recompiles the JSBSim command line interface for use with Node.js. We will need to develop JavaScript/C++ bindings to use the JSBSim classes as a real-time simulation library in the browser. This will be done using Embind through Emscripten (http://kripken.github.io/emscripten-site/docs/porting/connecting_cpp_and_javascript/embind.html).

The instrument panel will be rendered using SVGs and CSS in the browser. I plan to use this project as a starting point for the primary instruments: https://github.com/uw-ray/Skyhawk-Flight-Instruments. Other instruments will need to be developed to complete the instrument panel. These will be done with additional SVG images and CSS code, with JavaScript. 

I plan to use ClojureScript (https://github.com/clojure/clojurescript) to develop the client-side JavaScript code which will run the simulator. 

There is a large amount of navigational information that the simulator will need to accurately emulate the navigational instruments. For this I plan to use the FAA’s Coded Insturment Flight Procedure database (https://www.faa.gov/air_traffic/flight_info/aeronav/digital_products/cifp/). This is available for free download from the FAA’s website. The database follows the AIRINC 424 standard (http://dev.x-plane.com/update/data/424-15s.pdf). I plan to write a simple program to parse and upload the navigational information into Amazon’s DynamoDB service (https://aws.amazon.com/dynamodb/). 

To expose the data, I plan to use the Serverless framework (https://serverless.com/) to develop a REST API hosted through Amazon Web Services. The API will trigger an AWS Lambda function (https://aws.amazon.com/lambda/) using the API Gateway service (https://aws.amazon.com/api-gateway/). This public REST API can then be called from the simulator client through JavaScript. 