
// Helpers

// Helper function to create VectorString type
listToVector = function(l) {
    var nodes = new Module.VectorString();
    for (var i = 0; i < l.length; i++) {
        nodes.push_back(l[i]);
    }
    return nodes;
}

// Sim FDM wrapper class
class Sim {
  constructor() {
    this.requestAnimationFrame();
  }

  findAndGetParameter(fieldName) {
    return this.fdm.findAndGetParameter(fieldName);
  }

  getParameter(fieldName) {
    return this.fdm.getParameter(fieldName);
  }

  setParameter(fieldName, value) {
    return this.fdm.setParameter(fieldName, value);
  }

  setUpdateCallback(f) {
    this.updateCallback = f;
  }

  setInitializedCallback(f) {
    this.initializedCallback = f;
  }

  reset(script, nodes) {
    this.script = script;
    this.nodes = listToVector(nodes);
    this.willReset = true;
    this.running = false;
  }

  doReset() {
    this.willReset = false;
    this.lastTimestamp = null;
    this.lastElapsed = 0;
    if (!this.fdm) {
        this.fdm = new Module.FDM(this.script, this.nodes);
    } else {
        this.fdm.delete();
        this.fdm = new Module.FDM(this.script, this.nodes);
    }
    this.initializedCallback();
  }

  update(self, timestamp) {
    if (this.willReset) {
        this.doReset();
    }
    else if (this.running) {
        if (!Sim.lastTimestamp) {
            Sim.lastTimestamp = timestamp;
        }
        var elapsed = timestamp - Sim.lastTimestamp;
        Sim.lastElapsed = elapsed;
        Sim.lastTimestamp = timestamp;
        self.fdm.update(elapsed);
        self.updateCallback(elapsed);
    }
    self.requestAnimationFrame();
  }

  requestAnimationFrame() {
    var self = this;
    window.requestAnimationFrame(function(ts) {
        self.update(self, ts);
    });
  }

  start() {
    this.running = true;
  }
}

var SIM = null;
Module.print = function(s) {};
Module.onRuntimeInitialized = function() { 
  console.log("Module initialized");
  SIM = new Sim();
};