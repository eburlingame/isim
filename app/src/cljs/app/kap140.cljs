(ns app.kap140
  (:require [reagent.core :as reagent :refer [atom]]
            [goog.string :as gstring]
            [goog.string.format]
            [app.helpers :refer [scroll-increment!
                                 click-increment!]]))

(defn make-altitude-comp [alt]
  (if (>= alt 1000)
    [:div [:span (quot alt 1000)]
          [:span {:id "comma"} ","]
          [:span (gstring/format "%03d" (mod alt 1000))]]
    [:div [:span "  "]
          [:span {:id "comma"} " "]
          [:span (gstring/format "%-403d" (mod alt 1000))]]))

(defn kap140-autopilot [ap-roll-on ap-roll-attitude-mode ap-heading-selector-mode
                        ap-altitude-hold ap-altitude-hold-point]
  (let [ ]
    (fn [] 
      [:div {:id "kap140" :class "instrument"}
      [:img {:class "lcd" :src "img/kap140/kap140-ap-flag.svg" 
             :style {:display (if (= 0 @ap-roll-on) :none)}}]

      [:img {:class "lcd" :src "img/kap140/kap140-alert-flag.svg" :style {:display :none}}]
      [:img {:class "lcd" :src "img/kap140/kap140-fpm-flag.svg" :style {:display :none}}]
      [:img {:class "lcd" :src "img/kap140/kap140-ft-flag.svg" :style {:display :none}}]
      [:img {:class "lcd" :src "img/kap140/kap140-hpa-flag.svg" :style {:display :none}}]
      [:img {:class "lcd" :src "img/kap140/kap140-inhg-flag.svg" :style {:display :none}}]
      [:img {:class "lcd" :src "img/kap140/kap140-p-flag.svg" :style {:display :none}}]
      [:img {:class "lcd" :src "img/kap140/kap140-pt-flag.svg" :style {:display :none}}]
      [:img {:class "lcd" :src "img/kap140/kap140-r-flag.svg" :style {:display :none}}]
      [:img {:class "lcd" :src "img/kap140/kap140-pt-dn-flag.svg" :style {:display :none}}]
      [:img {:class "lcd" :src "img/kap140/kap140-pt-up-flag.svg" :style {:display :none}}]
      [:img {:class "lcd" :src "img/kap140/kap140-l-arm-flag.svg" :style {:display :none}}]
      [:img {:class "lcd" :src "img/kap140/kap140-r-arm-flag.svg" :style {:display :none}}]
  
      [:img {:id "ap-button" :class "button" :src "img/kap140/kap140-ap-button.svg"
             :on-click #(if (= @ap-roll-on 1)
                            (do (reset! ap-roll-on 0)
                                (reset! ap-roll-attitude-mode 0)
                                (reset! ap-altitude-hold 0))
                            (reset! ap-roll-on 1))}]
      
      [:img {:id "nav-button" :class "button" :src "img/kap140/kap140-nav-button.svg"}]

      [:img {:id "apr-button" :class "button" :src "img/kap140/kap140-apr-button.svg"}]
      [:img {:id "arm-button" :class "button" :src "img/kap140/kap140-arm-button.svg"}]
      [:img {:id "baro-button" :class "button" :src "img/kap140/kap140-baro-button.svg"}]
      [:img {:id "up-button" :class "button" :src "img/kap140/kap140-up-button.svg"}]
      [:img {:id "rev-button" :class "button" :src "img/kap140/kap140-rev-button.svg"}]
      [:img {:id "dn-button" :class "button" :src "img/kap140/kap140-dn-button.svg"}]

      [:img {:id "alt-button" :class "button" :src "img/kap140/kap140-alt-button.svg"
             :on-click #(do (reset! ap-altitude-hold 1))}]

      [:img {:id "hdg-button" :class "button" :src "img/kap140/kap140-hdg-button.svg"
             :on-click #(reset! ap-roll-attitude-mode 1)}]
  
      [:img {:id "inner-knob" :src "img/kap140/kap140-inner-knob.svg"
             :on-wheel #(scroll-increment! % 2 ap-altitude-hold-point 100)
             :on-click #(click-increment! % ap-altitude-hold-point -100)
             :on-context-menu #(click-increment! % ap-altitude-hold-point 100)}]
      [:img {:id "outer-knob" :src "img/kap140/kap140-outer-knob.svg"
             :on-wheel #(scroll-increment! % 2 ap-altitude-hold-point 1000)
             :on-click #(click-increment! % ap-altitude-hold-point -1000)
             :on-context-menu #(click-increment! % ap-altitude-hold-point 1000)}]

      [:div {:class "active-indicator" :id "active-indicator-left"}
        (if (= @ap-roll-on 1) 
            (cond (= @ap-roll-attitude-mode 0) "rol"
                  (= @ap-roll-attitude-mode 1) "hdg")
            "")]
      [:div {:class "armed-indicator" :id "armed-indicator-left"} " "]

      [:div {:class "active-indicator" :id "active-indicator-right"} " "]
      [:div {:class "armed-indicator" :id "armed-indicator-right"} " "]

      [:div {:id "altitude"} 
        (cond
         (= @ap-altitude-hold 1) (make-altitude-comp @ap-altitude-hold-point))]
  
      [:img {:id "base" :src "img/kap140/kap140-base.svg"}]])))
