(ns app.gamepad
  (:require [reagent.core :as reagent :refer [atom]]
            [app.instruments :as instruments])
  (:use [jayq.core :only [$ css html]]))

(def gamepad (atom []))

; $(window).on("gamepadconnected", function() {
;     hasGP = true;
;     $("#gamepadPrompt").html("Gamepad connected!");
;     console.log("connection event");
; });

; var gps = navigator.getGamepads();
; if (gps[0] != null)
; {
	; var x = -gps[0].axes[0];
	; var y = -gps[0].axes[1];
	; var t = 1 - (gps[0].axes[2] + 1) / 2;

	; fdm.setParameter("fcs/elevator-cmd-norm", y / 3);
	; fdm.setParameter("fcs/aileron-cmd-norm", x / 3);
	; fdm.setParameter("fcs/throttle-cmd-norm", t);
; }

(defn game-pad-connected []
	(. js/console (log "Hello!")))

((. (js/$ js/window) on) "gamepadconnected" game-pad-connected)