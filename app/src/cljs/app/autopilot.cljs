(ns app.autopilot
  (:require [reagent.core :as reagent :refer [atom]]))

(defn kap140-autopilot []
  [:div {:id "kap-140" :class "instrument"}
    [:img {:id "backgrond" :src "img/kap140/kap140-base.svg"}]])
