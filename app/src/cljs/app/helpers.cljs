(ns app.helpers)

(defn scroll-amt [evt]
  (.-deltaY (.-nativeEvent evt)))

(defn scroll-increment! [evt threshold to-swap amount]
  (do (.preventDefault evt)
      (swap! to-swap + 
            (cond 
                (< (scroll-amt evt) (- threshold)) amount
                (> (scroll-amt evt)  threshold) (- amount)))))

(defn click-increment! [evt to-swap amount]
  (do (.preventDefault evt)
      (swap! to-swap + amount)))

(defn swap-atom-values! [a b]
  (let [old-a @a]
    (do
      (reset! a @b)
      (reset! b old-a))))

(defn linmap [domain-low domain-hi value range-low range-hi]
  (+ range-low (* (- range-hi range-low) 
                  (/ (- value domain-low) (- domain-hi domain-low)))))

(defn constrain [low value high]
  (if (< low high)
      (cond 
        (<= value low) low
        (>= value high) high
        :else value)
      (constrain high value low)))

(defn map-over [lst value]
  (let [[domain-low range-low] (first lst)]
    (if (empty? (rest lst))
        domain-low
        (let [[domain-hi range-hi] (second lst)]
          (if (and 
                (<= value domain-hi)
                (>= value domain-low))
              (linmap domain-low domain-hi value range-low range-hi)
              (map-over (rest lst) value))))))

(defn linmap-const [domain-low domain-hi value range-low range-hi]
  (constrain range-low
             (linmap domain-low domain-hi value range-low range-hi)
             range-hi))

(defn rad-to-deg [rad]
  (* 180 (/ rad 3.14159)))

(defn deg-to-rad [deg]
  (* 3.14159 (/ deg 180)))