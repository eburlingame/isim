(ns app.skyhawk-instruments
  (:require [reagent.core :as reagent :refer [atom]])
  (:use [jayq.core :only [$ css html]]))

(def $interface ($ :#interface))

(defn rad-to-deg [rad]
  (* 180 (/ rad 3.14159)))

(defn constrain [low value high]
  (cond 
    (<= value low) low
    (>= value high) high
    :else value))

(def settings
  (clj->js
       {:off_flag true,
        :size 200,
        :showBox true,
        :showScrews true,
        :img_directory "flight-instruments/img/"}))

; Wrap the jQuery mangament of the component with react
; It's a little ugly, but we gain react's handling of the components
; state
(defn altimeter-component
  [altitude-feet pressure-in-hg]
  (let [jqry-init #(.flightIndicator js/$ "#altimeter" "altimeter" settings)
        ; Have to initialize the atom for the functions to work
        jqry-obj (reagent/atom (jqry-init))]
    (reagent/create-class 
      {:component-did-mount
        #(swap! jqry-obj
          ; Reinitialize the function on component mount and swap!
          (fn [] (jqry-init)))
       :component-will-mount
       #()
       :display-name "altimeter-component"
       :reagent-render
        (fn [altitude-feet pressure-in-hg]
          (. @jqry-obj (setAltitude altitude-feet))
          (. @jqry-obj (setPressure pressure-in-hg false))
          [:div {:id "altimeter" :class "instrument"}])
      })))

(defn attitude-component
  [pitch-rad roll-rad]
  (let [jqry-init #(.flightIndicator js/$ "#attitude" "attitude" settings)
        ; Have to initialize the atom for the functions to work
        jqry-obj (reagent/atom (jqry-init))]
    (reagent/create-class 
      {:component-did-mount
        #(swap! jqry-obj
          ; Reinitialize the function on component mount and swap!
          (fn [] (jqry-init)))
       :component-will-mount
       #()
       :display-name "attitude-component"
       :reagent-render
        (fn [pitch-rad roll-rad]
          (. @jqry-obj (setPitch (rad-to-deg pitch-rad)))
          (. @jqry-obj (setRoll (rad-to-deg roll-rad)))
          [:div {:id "attitude" :class "instrument"}])
      })))

(defn airspeed-component
  [airspeed-kts]
  (let [jqry-init #(.flightIndicator js/$ "#airspeed" "airspeed" settings)
        ; Have to initialize the atom for the functions to work
        jqry-obj (reagent/atom (jqry-init))]
    (reagent/create-class 
      {:component-did-mount
        #(swap! jqry-obj
          ; Reinitialize the function on component mount and swap!
          (fn [] (jqry-init)))
       :component-will-mount
       #()
       :display-name "airspeed-component"
       :reagent-render
        (fn [airspeed-kts]
          (. @jqry-obj (setAirSpeed airspeed-kts))
          [:div {:id "airspeed" :class "instrument"}])
      })))

(defn turn-coordinator-component
  [roll-rad slip]
  (let [jqry-init #(.flightIndicator js/$ "#turn_coordinator" "turn_coordinator" settings)
        ; Have to initialize the atom for the functions to work
        jqry-obj (reagent/atom (jqry-init))]
    (reagent/create-class 
      {:component-did-mount
        #(swap! jqry-obj
          ; Reinitialize the function on component mount and swap!
          (fn [] (jqry-init)))
       :component-will-mount
       #()
       :display-name "turn-coordinator-component"
       :reagent-render
        (fn [roll-rad slip]
          (. @jqry-obj (setTurn (constrain -25 (rad-to-deg roll-rad) 25)))
          (. @jqry-obj (setSlip slip))
          [:div {:id "turn_coordinator" :class "instrument"}])
      })))

(defn heading-indicator-component
  [heading-rad]
  (let [jqry-init #(.flightIndicator js/$ "#heading" "heading" settings)
        ; Have to initialize the atom for the functions to heading
        jqry-obj (reagent/atom (jqry-init))]
    (reagent/create-class 
      {:component-did-mount
        #(swap! jqry-obj
          ; Reinitialize the function on component mount and swap!
          (fn [] (jqry-init)))
       :component-will-mount
       #()
       :display-name "heading-indicator-component"
       :reagent-render
        (fn [heading-rad]
          (. @jqry-obj (setHeading (rad-to-deg heading-rad)))
          [:div {:id "heading" :class "instrument"}])
      })))

(defn vertspeed-component
  [vertspeed-fps]
  (let [jqry-init #(.flightIndicator js/$ "#variometer" "variometer" settings)
        ; Have to initialize the atom for the functions to work
        jqry-obj (reagent/atom (jqry-init))]
    (reagent/create-class 
      {:component-did-mount
        #(swap! jqry-obj
          ; Reinitialize the function on component mount and swap!
          (fn [] (jqry-init)))
       :component-will-mount
       #()
       :display-name "vertspeed-component"
       :reagent-render
        (fn [vertspeed-fps]
          (. @jqry-obj (setVario vertspeed-fps))
          [:div {:id "variometer" :class "instrument"}])
      })))