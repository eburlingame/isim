(ns app.app
  (:require [reagent.core :as reagent :refer [atom]]
            [app.instruments :as instruments]
            [app.skyhawk-instruments :refer [altimeter-component
                                             attitude-component
                                             airspeed-component
                                             turn-coordinator-component
                                             vertspeed-component]]
            [app.kap140 :refer [kap140-autopilot]]
            [app.helpers :refer [rad-to-deg deg-to-rad]]
            [app.data :refer [bearing-to-vor]]))

(defn log [msg]
  (.log js/console msg))

(def fields {
             :pitch            "attitude/pitch-rad"
             :roll             "attitude/roll-rad"
             :airspeed         "velocities/vc-kts"
             :vert-speed       "velocities/v-down-fps"
             :altitude-msl     "position/h-sl-ft"
             :latitude         "position/lat-gc-deg"
             :longitude        "position/long-gc-deg"
             :heading          "attitude/heading-true-rad"
             :slip             "gear/unit/slip-angle-deg"
             :prop-rpm         "propulsion/engine/propeller-rpm"
             :flaps-pos        "fcs/flap-pos-deg"
             :y-accel-fps      "accelerations/a-pilot-y-ft_sec2"
             :z-accel-fps      "accelerations/a-pilot-z-ft_sec2"
             })

(def controls {
             :elevator-cmd              "fcs/elevator-cmd-norm"
             :ailerion-cmd              "fcs/aileron-cmd-norm"
             :throttle-cmd              "fcs/throttle-cmd-norm"
             :mixture-cmd               "fcs/mixture-cmd-norm"
             :flaps-cmd                 "fcs/flap-cmd-norm"
             :ap-roll-attitude-mode     "ap/roll-attitude-mode"
             :ap-roll-on                "ap/autopilot-roll-on"
             :heading-bug               "guidance/specified-heading-rad"
             :ap-heading-selector-mode  "guidance/heading-selector-switch"
             :ap-altitude-hold          "ap/altitude_hold"
             :ap-altitude-hold-point    "ap/altitude_setpoint"})

(def start-sim-state {
                      :elevator-cmd 0
                      :ailerion-cmd 0
                      :throttle-cmd 1
                      :mixture-cmd  1
                      :heading-bug 5.236
                      :ap-roll-on 1
                      :ap-roll-attitude-mode 1
                      :ap-heading-selector-mode 1
                      :ap-altitude-hold 1
                      :ap-altitude-hold-point 4000})

(defn get-field [fieldname]
  (if (contains? controls fieldname)
      (controls fieldname)
      (fields fieldname)))

(def sim-state 
  (reduce-kv 
    (fn [m k field]
      (assoc m k (if (contains? start-sim-state k)
                     (atom (start-sim-state k))
                     (atom 0))))
    {} (merge fields controls)))

(def refresh-rate (reagent/atom 0))

(defn get-sim-value [field]
  (.getParameter js/SIM field))

(defn fetch-sim-state! []
  (doall
    (map 
      #(reset! (sim-state %) (get-sim-value (get-field %)))
      (keys (merge fields controls)))))

(defn apply-controls! []
  (doall
    (map 
      #(. js/SIM (setParameter (controls %) @(sim-state %)))
       (keys controls))))

(defn reset-control! [control value]
  (reset! (sim-state control) value))

(defn increment-control! [control by]
  (swap! (sim-state control) + by))

(defn floating-position [control total offset]
  (+ (- (/ total 2) offset) (* (- control) (/ total 2))))

(defn bind-keydown [caller]
  (.off (js/$ "body"))
  (.on (js/$ "body") "keydown" caller))

(defn control-cross-compnent [aileron elevator mouse-flying]
  (let [centerX 272
        centerY 250
        last-mouse-pos (atom [0 0])]
    (fn []
      [:svg 
             {:width 100
              :height 100
              :viewBox "0 0 150 100"
              :on-click #(swap! mouse-flying not)
              :on-mouse-move 
                #(do
                  (reset! last-mouse-pos [(.-screenX (.-nativeEvent %))
                                          (.-screenY (.-nativeEvent %))]))
              :style {:position "absolute"
                      :z-index 10
                      :top  (if @mouse-flying (- (second @last-mouse-pos) 172) (floating-position @elevator 500 50))
                      :left (if @mouse-flying (- (first @last-mouse-pos) 75) (floating-position (- @aileron) 540 50))}}
             [:path {:stroke-width 13
                     :stroke (if @mouse-flying "#F44336" "#fff")
                     :d "M42,50h66M75,17v66"}]])))

(defn bearing->normal [bearing]
  (if (> bearing 180)
    (- bearing 360)
    bearing))

(defn plus-360 [a b]
  (let [res (+ a b)]
    (cond
     (> res 360) (- res 360)
     (< res 0) (+ res 360)
     :else res)))

(defn get-ball-pos []
  (* 1 (/ @(sim-state :y-accel-fps)
            (if (< @(sim-state :z-accel-fps) 1)
                1
                @(sim-state :z-accel-fps)))))

(defn get-course-deviation [freq obs]
  (let [bearing (bearing-to-vor @(sim-state :latitude)
                                @(sim-state :longitude)
                                freq)]
      (if (nil? bearing)
          -50
          (let [bearing-diff (- 
                              (bearing->normal bearing)
                              (bearing->normal obs))]
            (if (< (Math/abs bearing-diff) 90)
              bearing-diff
              (- (bearing->normal (+ bearing 180))
                 (bearing->normal obs)))))))

(defn get-to-from [freq obs]
  (let [bearing (bearing-to-vor @(sim-state :latitude)
                                @(sim-state :longitude)
                                freq)]
      (if (nil? bearing)
          0
          (let [bearing-diff (- 
                              (bearing->normal bearing)
                              (bearing->normal obs))]
            (if (< (Math/abs bearing-diff) 90)
              1
              -1)))))

(def radio1-comm-stby-freq (atom 122.75))
(def radio1-comm-active-freq (atom 124.700))
(def radio1-nav-stby-freq (atom 117.0))
(def radio1-nav-active-freq (atom 114.3))

(def radio2-comm-stby-freq (atom 122.75))
(def radio2-comm-active-freq (atom 124.700))
(def radio2-nav-stby-freq (atom 118.2))
(def radio2-nav-active-freq (atom 114.3))

(def vor1-ring-deg (atom 30))
(def vor2-ring-deg (atom 30))
(def adf-ring-deg (atom 30))

(def mouse-flying (atom false))

(defn panel-component []
  [:div
      [control-cross-compnent
        (sim-state :ailerion-cmd)
        (sim-state :elevator-cmd)
        mouse-flying]
      [instruments/panel-background]
      [instruments/warning-lights]
      [instruments/audio-panel]
      [instruments/radio 1 radio1-comm-active-freq radio1-comm-stby-freq
                           radio1-nav-active-freq radio1-nav-stby-freq]
      [instruments/radio 2 radio2-comm-active-freq radio2-comm-stby-freq
                           radio2-nav-active-freq radio2-nav-stby-freq]
      [kap140-autopilot 
        (sim-state :ap-roll-on)
        (sim-state :ap-roll-attitude-mode)
        (sim-state :ap-heading-selector-mode)
        (sim-state :ap-altitude-hold)
        (sim-state :ap-altitude-hold-point)]
      [instruments/flap-control @(sim-state :flaps-pos) (sim-state :flaps-cmd)]
      [instruments/tachometer @(sim-state :prop-rpm)]
      [instruments/throttle @(sim-state :throttle-cmd)]
      [instruments/mixture @(sim-state :mixture-cmd)]
      [instruments/adf adf-ring-deg 0]
      [instruments/single-axis-vor vor2-ring-deg 
                                (get-course-deviation @radio2-nav-active-freq @vor2-ring-deg)
                                (get-to-from @radio2-nav-active-freq @vor2-ring-deg)]
      [instruments/two-axis-vor vor1-ring-deg
                                (get-course-deviation @radio1-nav-active-freq @vor1-ring-deg)
                                0
                                (get-to-from @radio1-nav-active-freq @vor1-ring-deg)
                                0]
      [airspeed-component @(sim-state :airspeed)]
      [attitude-component 
        @(sim-state :pitch)
        (- @(sim-state :roll))]
      [altimeter-component @(sim-state :altitude-msl) 29.92]
      [turn-coordinator-component @(sim-state :roll) (get-ball-pos)]
      [instruments/heading-indicator @(sim-state :heading) (sim-state :heading-bug)]
      [vertspeed-component (- @(sim-state :vert-speed))]
      [:h3 "Refresh rate: " @refresh-rate]
      [:h3 "Latitude: " @(sim-state :latitude)]
      [:h3 "Longitude: " @(sim-state :longitude)]
      [:h3 "Deviation: " (get-course-deviation @radio2-nav-active-freq @vor2-ring-deg)]])

(def key-bindings {
     "ArrowUp"      #(increment-control! :elevator-cmd 0.003)
     "ArrowDown"    #(increment-control! :elevator-cmd -0.003)
     "ArrowLeft"    #(increment-control! :ailerion-cmd -0.003)
     "ArrowRight"   #(increment-control! :ailerion-cmd 0.003)
     " "            #(do 
                      (reset-control! :ailerion-cmd 0)
                      (reset-control! :elevator-cmd 0))
     "Home"         #(do 
                      (reset-control! :ailerion-cmd 0)
                      (reset-control! :elevator-cmd 0))
     "F1"           #(reset-control! :throttle-cmd 0)
     "F2"           #(increment-control! :throttle-cmd -0.2)
     "F3"           #(increment-control! :throttle-cmd 0.2)
     "F4"           #(reset-control! :throttle-cmd 1)

     "F5"           #(reset-control!     :flaps-cmd 0)
     "F6"           #(increment-control! :flaps-cmd -0.333)
     "F7"           #(increment-control! :flaps-cmd 0.333)
     "F8"           #(reset-control! :flaps-cmd 1)
     "r"            #(do 
                      (.setParameter js/SIM "ic/lat-gc-deg" 36.0041225)
                      (.setParameter js/SIM "ic/long-gc-deg" -120.1423608)
                      (.setParameter js/SIM "ic/h-sl-ft" 4500)
                      (.setParameter js/SIM "simulation/reset" 0))
     "z"            #(do 
                      (reset-control! :heading-bug 210)
                      (reset-control! :ap-roll-attitude-mode 1))})

(defn keypress-handler [e]
  (if (contains? key-bindings (.-key e))
    (do 
      (.preventDefault e)
      ((key-bindings (.-key e))))))

(defn container-component [body] 
  (fn []
    (bind-keydown keypress-handler)
    [panel-component]))

(defn sim-update [elapsed]
    (reset! refresh-rate elapsed)
    (.setParameter js/SIM "ap/altitude-pid-kp" 0.05)
    (.setParameter js/SIM "ap/altitude-pid-ki" 0.0035)
    (.setParameter js/SIM "ap/altitude-pid-kd" 0.02)
    (apply-controls!)
    (fetch-sim-state!))

(defn fdm-initialized []
  (do 
    (.log js/console "Initialized.")
    (.setParameter js/SIM "ic/lat-gc-deg" 36.0041225)
    (.setParameter js/SIM "ic/long-gc-deg" -120.1423608)
    (.setParameter js/SIM "ic/h-sl-ft" 4500)
    (.setParameter js/SIM "simulation/reset" 0)))

(defn initialize []
  (.log js/console "Initializing...")
  (.setUpdateCallback js/SIM sim-update)
  (.setInitializedCallback js/SIM fdm-initialized)
  (.reset js/SIM 
    "scripts/c172_cruise.xml"
    (clj->js (concat (vals fields) (vals controls))))
  (.start js/SIM)
  (reagent/render-component
    [container-component]
    (.getElementById js/document "container")))

(defn init []
  (if (= js/SIM nil)
      (.requestAnimationFrame js/window init)
      (initialize)))