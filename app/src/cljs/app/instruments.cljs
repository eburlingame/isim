(ns app.instruments 
  (:require [reagent.core :as reagent :refer [atom]]
            [reanimated.core :as anim]
            [goog.string :as gstring]
            [goog.string.format]
            [app.helpers :refer [swap-atom-values!
                                 linmap
                                 constrain
                                 map-over
                                 linmap-const
                                 rad-to-deg
                                 deg-to-rad
                                 scroll-amt
                                 scroll-increment!]]))

(defn panel-background []
  [:img {:class "panel-background" :src "img/instrument_panel_base.svg"}])

(defn warning-lights []
  [:img {:id "warning-lights" :src "img/warning-lights/warning-lights.svg"}])

(defn audio-panel []
  [:img {:id "audio-panel" :src "img/audio-panel/audio-panel.svg"}])

(defn flap-control [position-deg setting-norm]
  [:div {:id "flap-control"
         :on-click #(swap! setting-norm + 0.333)
         :on-context-menu #(do (.preventDefault %) (swap! setting-norm - 0.333))}
    [:img {:id "lever" 
           :src "img/flap-controls/flap-controls-handle.svg"
           :style {:top (cond 
                          (< @setting-norm 0.333) 0
                          (< @setting-norm 0.666) 47
                          (< @setting-norm 1.0)   76
                          :else           122)}}]
    [:img {:id "indicator"
           :src "img/flap-controls/flap-controls-indicator.svg"
           :style {:top (map-over [[0 0]
                                   [10 42]
                                   [20 72]
                                   [40 178]] position-deg)}}]
    [:img {:id "backgrond" :src "img/flap-controls/flap-controls-base.svg"}]])


(defn heading-indicator [heading-rad heading-bug-rad]
  [:div {:id "heading-indicator" :class "instrument"}
    [:img {:id "bug" :src "img/heading-indicator/heading-indicator-bug.svg"
           :style {:transform (str "rotate(" (- @heading-bug-rad heading-rad) "rad)")}}]
    [:img {:id "bug-knob" :src "img/heading-indicator/heading-indicator-bug-knob.svg"
           :on-wheel #(do (.preventDefault %)
                          (swap! heading-bug-rad + 
                            (cond 
                                (< (scroll-amt %) -2) (deg-to-rad -1)
                                (> (scroll-amt %)  2) (deg-to-rad 1))))
           :on-click #(swap! heading-bug-rad - (deg-to-rad 1))
           :on-context-menu #(do (.preventDefault %)
                                 (swap! heading-bug-rad + (deg-to-rad 1)))}]
    [:img {:id "calib-knob" :src "img/heading-indicator/heading-indicator-calib-knob.svg"}]
    [:img {:id "ring" :src "img/heading-indicator/heading-indicator-ring.svg"
           :style {:transform (str "rotate(" (- heading-rad) "rad)")}}]
    [:img {:id "base" :src "img/heading-indicator/heading-indicator-base.svg"}]])

(defn tachometer [rpm]
  [:div {:id "tachometer" :class "instrument"}
    [:img {:id "needle" 
           :src "img/tachometer/tachometer-needle.svg"
           :style {:transform (str "rotate(" (linmap 0 3500 rpm -120 125) "deg)")}}]
    [:img {:id "base" :src "img/tachometer/tachometer-base.svg"}]])

(defn adf [ring-deg honing-deg]
  [:div {:id "adf" :class "instrument"
         :on-wheel #(do (.preventDefault %)
                (swap! ring-deg + (cond 
                                      (< (scroll-amt %) -2) -1
                                      (> (scroll-amt %)  2)  1)))
         :on-click #(swap! ring-deg - 1)
         :on-context-menu #(do (.preventDefault %) (swap! ring-deg + 1))}
    [:img {:id "arrow" 
           :src "img/adf/adf-arrow.svg"
           :style {:transform (str "rotate(" honing-deg "deg)")}}]
    [:img {:id "ring" 
           :src "img/adf/adf-ring.svg"
           :style {:transform (str "rotate(" (- @ring-deg) "deg)")}}]
    [:img {:id "base" :src "img/adf/adf-base.svg"}]])

(defn plus-360 [a b]
  (let [res (+ a b)]
    (cond
     (> res 360) (- res 360)
     (< res 0) (+ res 360)
     :else res)))

(defn minus-360 [a b]
  (let [res (- a b)]
    (cond
     (> res 360) (- res 360)
     (< res 0) (+ res 360)
     :else res)))


(defn single-axis-vor [ring-deg needle-deg to-from-amt]
  [:div {:id "single-axis-vor" :class "instrument"
         :on-wheel #(do (.preventDefault %)
                        (swap! ring-deg plus-360 (cond 
                                      (< (scroll-amt %) -2) -1
                                      (> (scroll-amt %)  2)  1)))
         :on-click #(swap! ring-deg minus-360 1)
         :on-context-menu #(do (.preventDefault %) (swap! ring-deg + 1))}
    [:img {:id "arrow" :src "img/single-axis-vor/single-axis-vor-arrow.svg"}]
    [:img {:id "nav-mask" 
           :src "img/single-axis-vor/single-axis-vor-nav-mask.svg"}]
    [:img {:id "nav-flag" :src "img/single-axis-vor/single-axis-vor-nav-flag.svg"
           :style {:top (linmap -1 1 to-from-amt -2.4 2.4)}}]
    [:img {:id "needle"
           :src "img/single-axis-vor/single-axis-vor-vert-needle.svg"
           :style {:transform (str "rotate(" (linmap-const 10 -10 needle-deg -25 25) "deg)")}}]

    [:img {:id "ring" 
           :src "img/single-axis-vor/single-axis-vor-ring.svg"
           :style {:transform (str "rotate(" (- @ring-deg) "deg)")}}]
    [:img {:id "base" :src "img/single-axis-vor/single-axis-vor-base.svg"}]])

(defn two-axis-vor [ring-deg vert-needle-deg horiz-needle-deg to-from-amt warn-amt]
  [:div {:id "two-axis-vor" :class "instrument"
         :on-wheel #(do (.preventDefault %)
                (swap! ring-deg plus-360 (cond 
                                      (< (scroll-amt %) -2) -1
                                      (> (scroll-amt %)  2)  1)))
         :on-click #(swap! ring-deg minus-360 1)
         :on-context-menu #(do (.preventDefault %) (swap! ring-deg + 1))}
    [:img {:id "arrow" :src "img/two-axis-vor/two-axis-vor-arrow.svg"}]
    [:img {:id "masks" 
           :src "img/two-axis-vor/two-axis-vor-masks.svg"}]
    [:img {:id "nav-flag" :src "img/two-axis-vor/two-axis-vor-nav-flag.svg"
           :style {:top (linmap-const -1 1 to-from-amt -2.4 2.4)}}]
    [:img {:id "gs-flag" :src "img/two-axis-vor/two-axis-vor-gs-flag.svg"
           :style {:top (linmap-const 1 0 warn-amt 0 2.4)}}]
    [:img {:id "vert-needle"
           :src "img/two-axis-vor/two-axis-vor-vert-needle.svg"
           :style {:transform (str "rotate(" (linmap-const 10 -10 vert-needle-deg -34 34) "deg)")}}]
    [:img {:id "horiz-needle"
           :src "img/two-axis-vor/two-axis-vor-horiz-needle.svg"
           :style {:transform (str "rotate(" (linmap-const 4 -4 horiz-needle-deg -24.5 24.5) "deg)")}}]
    [:img {:id "ring" 
           :src "img/two-axis-vor/two-axis-vor-ring.svg"
           :style {:transform (str "rotate(" (- @ring-deg) "deg)")}}]
    [:img {:id "base" :src "img/two-axis-vor/two-axis-vor-base.svg"}]])


(defn radio [id comm-active-freq comm-stby-freq nav-active-freq nav-stby-freq]
  [:div {:id (str "radio" id) :class "radio"}
    [:img {:id "comm-button" :src "img/radio/radio-comm-button.svg"
           :on-click #(swap-atom-values! comm-active-freq comm-stby-freq)}]
    [:img {:id "comm-volume-knob" :src "img/radio/radio-volume-knob.svg"
           :on-wheel #(do (.preventDefault %) (.log js/console "scrolled"))}]
    [:img {:id "nav-volume-knob" :src "img/radio/radio-volume-knob.svg"
           :on-wheel #(do (.preventDefault %) (.log js/console "scrolled"))}]

    [:div {:id "comm-active-frequency" :class "frequency"}
           (gstring/format "%.3f" @comm-active-freq)]

    [:div {:id "comm-stby-frequency" :class "frequency"}
           (gstring/format "%.3f" @comm-stby-freq)]

    [:div {:id "nav-active-frequency" :class "frequency"} (gstring/format "%.3f" @nav-active-freq)]
    [:div {:id "nav-stby-frequency" :class "frequency"} (gstring/format "%.3f" @nav-stby-freq)]

    [:img {:id "comm-inner-knob" :src "img/radio/radio-inner-knob.svg"
           :on-wheel #(do (.preventDefault %)
                (swap! comm-stby-freq - (cond 
                                          (< (scroll-amt %) -2) -0.025
                                          (> (scroll-amt %)  2)  0.025)))
           :on-click #(swap! comm-stby-freq + 0.025)
           :on-context-menu #(do (.preventDefault %) (swap! comm-stby-freq - 0.025))}]

    [:img {:id "comm-outer-knob" :src "img/radio/radio-outer-knob.svg"
           :on-wheel #(do (.preventDefault %)
                          (swap! comm-stby-freq - (cond 
                                                    (< (scroll-amt %) -2) -1
                                                    (> (scroll-amt %)  2)  1)))
           :on-click #(swap! comm-stby-freq - 1)
           :on-context-menu #(do (.preventDefault %) (swap! comm-stby-freq + 1))}]

    [:img {:id "nav-inner-knob" :src "img/radio/radio-inner-knob.svg"
           :on-wheel #(do (.preventDefault %)
                (swap! nav-stby-freq - (cond 
                                          (< (scroll-amt %) -2) -0.025
                                          (> (scroll-amt %)  2)  0.025)))
           :on-click #(swap! nav-stby-freq - 0.025)
           :on-context-menu #(do (.preventDefault %) (swap! nav-stby-freq + 0.025))}]
    [:img {:id "nav-outer-knob" :src "img/radio/radio-outer-knob.svg"
           :on-wheel #(do (.preventDefault %)
                          (swap! nav-stby-freq - (cond 
                                                  (< (scroll-amt %) -2) -1
                                                  (> (scroll-amt %)  2)  1)))
           :on-click #(swap! nav-stby-freq - 1)
           :on-context-menu #(do (.preventDefault %) (swap! nav-stby-freq + 1))}]

    [:img {:id "nav-button" :src "img/radio/radio-nav-button.svg"
           :on-click #(swap-atom-values! nav-active-freq nav-stby-freq)}]

    [:img {:id "base" :src "img/radio/radio-base.svg"}]])

(defn throttle [setting]
  [:div {:id "throttle" :class "instrument"}
    [:img {:id "handle" 
           :src "img/throttle/throttle-handle.svg"
           :style {:top (linmap-const 0 1 setting 0 -50)}}]
    [:img {:id "base" :src "img/throttle/throttle-base.svg"}]])

(defn mixture [setting]
  [:div {:id "mixture" :class "instrument"}
    [:img {:id "handle" 
           :src "img/mixture/mixture-handle.svg"
           :style {:top (linmap-const 0 1 setting 0 -50)}}]
    [:img {:id "base" :src "img/mixture/mixture-base.svg"}]])
