(ns app.data
	(:require [ajax.core :refer [GET POST]]
			  [goog.string :as gstring]
			  [reagent.core :as reagent :refer [atom]]))

(def vors (atom []))
(def vor-sectors (atom #{}))

(defn m->nm [m]
	(* m 0.000539957))

(defn distance-between [p1 p2]
	(.getDistance js/geolib (clj->js p1)
							(clj->js p2)))

(defn bearing-between [p1 p2]
	(.getBearing js/geolib (clj->js p1)
						   (clj->js p2)))

(defn find-vor [freq]
	(first (filter #(= (% :vor-ndb-freq) (float freq)) @vors)))

(defn keywordize [l]
	(map #(zipmap (map keyword (keys %))
				  (vals %)) l))

(defn format-json [json]
	(-> json
		js->clj
		keywordize))

(defn add-to-seen-sectors [lat-sec lon-sec]
	(swap! vor-sectors clojure.set/union #{[lat-sec lon-sec]}))

(defn handler [response lat-sec lon-sec]
	(do (swap! vors #(concat % (format-json response)))
		(add-to-seen-sectors lat-sec lon-sec)))

(defn error-handler [{:keys [status status-text]} lat-sec lon-sec]
  (do 
  	(.log js/console (str "Blacklisting " lat-sec ", " lon-sec))
  	(add-to-seen-sectors lat-sec lon-sec)))

(defn sector-handler [handler lat-sec lon-sec]
	#(handler % lat-sec lon-sec))

(defn get-data [lat-sec lon-sec]
	(if (not (contains? @vor-sectors [lat-sec lon-sec]))
		(GET (str "/data/" (gstring/format "%.1f" lat-sec) "/" (gstring/format "%.1f" lon-sec) "/vors.json")
			{
			:handler (sector-handler handler lat-sec lon-sec)
			:response-format :json
	        :error-handler (sector-handler error-handler lat-sec lon-sec)})
		nil))

(defn trun
  ([v] (quot v 1))
  ([v g] (* g (quot (/ v g) 1))))

(defn latlon-sector [lat lon]
	[(trun lat 10) (trun lon 10)])

(defn bearing-to-vor [lat lon freq]
	(let [[lat-sec lon-sec] (latlon-sector lat lon)
		  san-freq (/ (trun (* 100 (+ freq 0.005))) 100)] ; Avoid float rounding errors like 117.00999999999
		(if (not (contains? @vor-sectors [lat-sec lon-sec]))

			(do (get-data (- lat-sec 10) (- lon-sec 10))
				(get-data (- lat-sec 10) lon-sec)
				(get-data (- lat-sec 10) (+ lon-sec 10))

				(get-data lat-sec (- lon-sec 10))
				(get-data lat-sec lon-sec)
				(get-data lat-sec (+ lon-sec 10))

				(get-data (+ lat-sec 10) (- lon-sec 10))
				(get-data (+ lat-sec 10) lon-sec)
				(get-data (+ lat-sec 10) (+ lon-sec 10)) 

				(.log js/console (clj->js [lat-sec lon-sec]))

				nil)

			(let [vor (find-vor san-freq)]
				(if (nil? vor)
					(.log js/console san-freq)
					(bearing-between {:latitude lat
									  :longitude lon}
									 {:latitude (vor :vor-latitude)
							  		  :longitude (vor :vor-longitude)}))))))

(defn get-vor []
	(bearing-to-vor 36 -120 114.3))

