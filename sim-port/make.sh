#!/bin/bash
set -e 

# export EMCC_DEBUG=0
EM_SDK="/home/eric/Libraries/emsdk/emscripten/1.37.28/"

cd JSBSim

# $EM_SDK/emmake make clean
$EM_SDK/emmake make

# compile

$EM_SDK/emcc \
src/SimInterface.cpp \
src/FGJSBBase.o \
src/math/FGFunction.o \
src/math/FGPropertyValue.o \
src/math/FGLocation.o \
src/math/FGRealValue.o \
src/math/FGMatrix33.o \
src/math/FGTable.o \
src/math/FGCondition.o \
src/math/FGQuaternion.o \
src/math/FGColumnVector3.o \
src/models/FGExternalForce.o \
src/models/atmosphere/FGMars.o \
src/models/atmosphere/FGMSISData.o \
src/models/atmosphere/FGMSIS.o \
src/models/FGPropulsion.o \
src/models/FGAerodynamics.o \
src/models/FGBuoyantForces.o \
src/models/FGAuxiliary.o \
src/models/FGLGear.o \
src/models/FGOutput.o \
src/models/FGModel.o \
src/models/FGExternalReactions.o \
src/models/FGAircraft.o \
src/models/FGAtmosphere.o \
src/models/FGPropagate.o \
src/models/flight_control/FGSummer.o \
src/models/flight_control/FGDeadBand.o \
src/models/flight_control/FGSensor.o \
src/models/flight_control/FGFCSComponent.o \
src/models/flight_control/FGFCSFunction.o \
src/models/flight_control/FGAccelerometer.o \
src/models/flight_control/FGGain.o \
src/models/flight_control/FGActuator.o \
src/models/flight_control/FGFilter.o \
src/models/flight_control/FGSwitch.o \
src/models/flight_control/FGPID.o \
src/models/flight_control/FGKinemat.o \
src/models/FGFCS.o \
src/models/FGGroundReactions.o \
src/models/FGMassBalance.o \
src/models/propulsion/FGTurbine.o \
src/models/propulsion/FGPropeller.o \
src/models/propulsion/FGNozzle.o \
src/models/propulsion/FGElectric.o \
src/models/propulsion/FGForce.o \
src/models/propulsion/FGEngine.o \
src/models/propulsion/FGTurboProp.o \
src/models/propulsion/FGTank.o \
src/models/propulsion/FGRocket.o \
src/models/propulsion/FGPiston.o \
src/models/propulsion/FGThruster.o \
src/models/FGInput.o \
src/models/FGGasCell.o \
src/models/FGInertial.o \
src/simgear/xml/easyxml.o \
src/simgear/xml/xmlrole.o \
src/simgear/xml/xmlparse.o \
src/simgear/xml/xmltok.o \
src/simgear/props/props.o \
src/FGFDMExec.o \
src/FGState.o \
src/initialization/FGInitialCondition.o \
src/initialization/FGTrimAxis.o \
src/initialization/FGTrim.o \
src/input_output/FGfdmSocket.o \
src/input_output/FGPropertyManager.o \
src/input_output/FGXMLElement.o \
src/input_output/FGGroundCallback.o \
src/input_output/FGScript.o \
src/input_output/FGXMLParse.o \
--embed-file scripts/c1721.xml \
--embed-file scripts/c1722.xml \
--embed-file scripts/c1723.xml \
--embed-file scripts/c172_cruise_8K.xml \
--embed-file scripts/c172_cruise.xml \
--embed-file aircraft/c172x/c172x.xml \
--embed-file aircraft/c172x/c172ap.xml \
--embed-file aircraft/c172x/output.xml \
--embed-file aircraft/c172x/reset00.xml \
--embed-file aircraft/c172x/reset01.xml \
--embed-file engine/eng_io320.xml \
--embed-file engine/prop_Clark_Y7570.xml \
--embed-file systems/Autopilot.xml \
--embed-file systems/BLC.xml \
--embed-file systems/catapult.xml \
--embed-file systems/FCS-pitch.xml \
--embed-file systems/FCS-roll.xml \
--embed-file systems/FCS-yaw.xml \
--embed-file systems/flaps.xml \
--embed-file systems/gear.xml \
--embed-file systems/GNCUtilities.xml \
--embed-file systems/holdback.xml \
--embed-file systems/hook.xml \
--embed-file systems/NWS.xml \
--embed-file systems/refuel.xml \
--embed-file systems/speedbrakes.xml \
--bind \
-o ../index.js -I./src \
-s DEMANGLE_SUPPORT=1 \
-O2 \
-s ASSERTIONS=1

cd ..
# Replace index.js.mem -> js/index.js.mem so it will load it from the proper url
echo "Replace index.js.mem -> js/index.js.mem"
sed -i 's/index\.js\.mem/js\/index\.js\.mem/g' index.js
echo "Copying index.js and index.js.mem to ../app/resources/js/"
cp index.js index.js.mem ../app/resources/js/.