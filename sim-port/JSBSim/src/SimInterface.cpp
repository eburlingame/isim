//
// Created by eric on 12/4/17.
//
#include <FGFDMExec.h>
#include <sys/time.h>
#include <emscripten/bind.h>

class FDM {
public:

    double frame_duration;

    double new_five_second_value;
    double actual_elapsed_time;
    double initial_seconds;
    double current_seconds;
    double paused_seconds;
    double sim_time;
    double sim_lag_time;
    double cycle_duration;
    long sleep_nseconds;

    bool was_paused;

    JSBSim::FGFDMExec *fdm;

    std::map <string, JSBSim::FGPropertyManager*> node_map;

    FDM(string script_name, std::vector<string> field_names) {
        new_five_second_value = 0.0;
        actual_elapsed_time = 0;
        initial_seconds = 0;
        current_seconds = 0.0;
        paused_seconds = 0.0;
        sim_time = 0.0;
        sim_lag_time = 0;
        cycle_duration = 0.0;
        sleep_nseconds = 0;
        was_paused = false;

        fdm = new JSBSim::FGFDMExec();
        cerr << "Constructor called" << endl;

        fdm->SetAircraftPath("aircraft");
        fdm->SetEnginePath("engine");
        fdm->SetSystemsPath("systems");
        fdm->GetPropertyManager()->Tie("simulation/frame_start_time", &actual_elapsed_time);
        fdm->GetPropertyManager()->Tie("simulation/cycle_duration", &cycle_duration);

        for (int i = 0; i < field_names.size(); i++) {
            if (fdm->GetPropertyManager()->HasNode(field_names[i])) {
                // cerr << "Field " << field_names[i] << " found" << endl;
                node_map[field_names[i]] = fdm->GetPropertyManager()->GetNode(field_names[i]);
            } else {
                node_map.erase(field_names[i]);
                cerr << "Field " << field_names[i] << " could not be found in the property manager" << endl;
            }
        }

        bool result = fdm->LoadScript(script_name);

        if (!result) {
            cerr << "Script file " << script_name << " was not successfully loaded" << endl;
            delete fdm;
            exit(-1);
        }

        initial_seconds = this->getcurrentseconds();
        actual_elapsed_time = 0.01;
        cycle_duration = 0.01;
        frame_duration = 0.01;
    }

    void initAircraft(string AircraftName, string ResetName) {

        if (!fdm->LoadModel("aircraft",
                            "engine",
                            "systems",
                            AircraftName)) {
            cerr << "  JSBSim could not be started" << endl << endl;
            delete fdm;
            exit(-1);
        }

        JSBSim::FGInitialCondition *IC = fdm->GetIC();
        if (!IC->Load(ResetName)) {
            delete fdm;
            cerr << "Initialization unsuccessful" << endl;
            exit(-1);
        }
    }

    void update(double elapsed) {
        bool result;

        if (was_paused) {
            initial_seconds += paused_seconds;
            was_paused = false;
        }
        current_seconds = getcurrentseconds();                      // Seconds since 1 Jan 1970
        actual_elapsed_time = current_seconds - initial_seconds;    // Real world elapsed seconds since start
        sim_lag_time = actual_elapsed_time - fdm->GetSimTime(); // How far behind sim-time is from actual
        // elapsed time.
        for (int i = 0; i < (int) (sim_lag_time / frame_duration); i++) {  // catch up sim time to actual elapsed time.
            result = fdm->Run();
            cycle_duration = getcurrentseconds() - current_seconds;   // Calculate cycle duration
            current_seconds = getcurrentseconds();                    // Get new current_seconds
            if (fdm->Holding()) break;
        }

        if (fdm->GetSimTime() >= new_five_second_value) { // Print out elapsed time every five seconds.
            cout << "Simulation elapsed time: " << fdm->GetSimTime() << endl;
            new_five_second_value += 5.0;
        }
    }

    void setParameter(std::string param, double input) {
        if (node_map.count(param) > 0) {
            node_map[param]->setDoubleValue(input);
        } else {
            if (findAndGetParameter(param) != -1) {
                node_map[param]->setDoubleValue(input);
            }
        }
    }

    double getParameter(std::string param) {
        if (node_map.count(param) > 0) {
            return node_map[param]->getDoubleValue();
        }
        return findAndGetParameter(param);
    }

    double findAndGetParameter(std::string param) {
        if (fdm->GetPropertyManager()->HasNode(param)) {
            node_map[param] = fdm->GetPropertyManager()->GetNode(param);
            cerr << "Field " << param << " has been added to the active nodes" << endl;
            return node_map[param]->getDoubleValue();
        }
        return -1;
    }

    double getcurrentseconds(void) {
        struct timeval tval;
        struct timezone tz;

        gettimeofday(&tval, &tz);
        return (tval.tv_sec + tval.tv_usec * 1e-6);
    }

    ~FDM()
    {
        cerr << "Destructor called" << endl;
        delete fdm;
    }
};

// Binding code
EMSCRIPTEN_BINDINGS(fdm_class) {
    emscripten::register_vector<std::string>("VectorString");
    emscripten::class_<FDM>("FDM")
        .constructor<std::string, std::vector<string>>()
        .function("update", &FDM::update)
        .function("setParameter", &FDM::setParameter)
        .function("getParameter", &FDM::getParameter)
        .function("findAndGetParameter", &FDM::findAndGetParameter);
}
