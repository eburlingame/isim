---
geometry: margin=2cm
title: In-Browser Instrument Flight Simulator
subtitle: Cal Poly CSC Senior Project, Spring 2018
author: Eric Burlingame
date: June 1st, 2018
highlight-style: pygments
template: eisvogel.tex
---

# In-Browser Instrument Flight Simulator

## Objective

The goal of this project is to design and develop an in-browser flight simulator aimed at pilots learning to fly on instruments. This project centers around an accurate instrument panel based on a very common training aircraft, the Cessna 172. A user will be able to "fly" the simulated aircraft with a mouse or joystick, and also interact with the instruments on the panel (radios, VORS, engine controls, etc.). The tool is designed to familiarize the pilot with the aircraft instruments, and gain some experience flying by references to instruments alone.

Unlike traditional flight simulators, this project will not include a 3D graphics system. This is done to keep the computational cost low (as the system runs in a browser), and to promote the tool's use as an instrument trainer. In standard instrument training pilots wear a view-limiting device to prevent him or her from seeing out the aircraft's front window. The goal of this project is to emulate that type of training.

## Implementation

### Overview

The simulator's implementation makes up 3 major components: the web application itself, the JavaScript (JS) port of the JSBSim library, and an ARINC 424 parser. Each of these components will be discussed in the following sections. 

### Programming Languages

Clojure and ClojureScript were chosen for this project because of their support and interoperability. Clojure is a functional Lisp variant that runs using the Java Virtual Machine. The ARINC 424 parser that processes aeronautical information was written entirely in Clojure. ClojureScript is a variant of Clojure that compiles into JavaScript via Google's Closure compiler. As a functional language, ClojureScript explicitly manages side-effects and state mutation. This makes it a good choice for a relatively complex client-side code that would traditionally be written in JS. 

### Flight Dynamics Model

#### JSBSim

The backbone of this application is the flight dynamics model. This is a sophisticated physics model that simulates the flight characteristics of the aircraft being flown. This software component is responsible for tracking the position of the aircraft and integrating the physics that affect it over time. The web application will need to continuously read data from this running model (latitude, longitude, airspeed, attitude, etc.), as well as write various values (control inputs, throttle setting, flaps configuration, etc.).

This component of the simulator has considerable complexity. The physics governing the flight of aircraft require a very complex physics engine. Additionally, the training value of a simulator is determined largely by how good its model is. Pilots are taught to fly the aircraft at specific speeds and expect the aircraft to respond in a particular manner. For instance, a pilot may fly the aircraft at a constant airspeed and expect the aircraft to descend at a certain rate. Any disparity between the simulation model and the actual aircraft may become distracting to the trainee, and could lead to the formation of bad habits. 

Because of this, I decided to utilize an existing flight dynamics model instead of creating one myself. I do not have the developer time nor the expertise to implement a simulation library by myself. This allows me to leverage the reliability of an existing code base that has already been written and tested. Additionally, many flight dynamics libraries come with configurations for common aircraft. This ensures that the handling characteristics of the aircraft has already been verified by a third party. 

Though there are very few open-source flight simulation libraries, I was able to find one that fit this project's needs. JSBSim is an open-source C++ library that serves as a fully-feature flight dynamics model. It can be run either as a standalone executable (usually for research purposes), or be integrated as a C++ library in a larger program. For instance, the fully-featured desktop flight simulator [FlightGear](flightgear.org) uses JSBSim as its underlying flight dynamics model.

The library is written entirely in C++ however, which cannot easily be target a web browser environment. Fortunately there is another tool, Emscripten, that allows us to port the library. 

#### Emscripten and Embind

Emscripten is a LLVM to JavaScript compiler. From their website: 

> Emscripten is an LLVM-to-JavaScript compiler. It takes LLVM bitcode - which can be generated from C/C++, using llvm-gcc (DragonEgg) or clang, or any other language that can be converted into LLVM - and compiles that into JavaScript, which can be run on the web (or anywhere else JavaScript can run).

The generated JavaScript code leverages the `asm.js` library to virtualize the target environment. Surprisingly, it is relatively straightforward to compile the C++ source into a JS library.  

The JSBSim repository comes with existing `CMake` targets to manage the build. Emscripten hooks into the existing Automake / CMake build process for compiling the library from source. Emscripten provides its own `emmake` command which compiles the C++ source to LLVM bytecode. That bytecode is then compiled to JS using `emcc`, Emscripten's `gcc` counterpart. Like `gcc`, you can also specify an optimization level using the `-On` flag.

Here is a preview of the build script used to build the library:

```{.sh}
EM_SDK="/[path]/[to]/emsdk/emscripten/1.37.28/"

cd JSBSim

# autogen
./autogen.sh 

# configure
$EM_SDK/emconfigure ./configure

# make
$EM_SDK/emmake make clean
$EM_SDK/emmake make

# compile llvm -> js
$EM_SDK/emcc \
src/SimInterface.cpp \
src/FGJSBBase.o \
 
[... other *.o files ...]

src/input_output/FGXMLParse.o \
--embed-file scripts/c1721.xml \

[... other embedded files ...]

--bind \
-o ../index.js -I./src \
-s DEMANGLE_SUPPORT=1 \
-O2 \
-s ASSERTIONS=1
``` 

The main steps are:

1. `autogen.sh`, this configures and generates the Makefile
2. `$EM_SDK/emconfigure ./configure`, which configures the make
3. `emmake make clean`: Cleans the build
4. `emmake make` : Call to make from emmake, which produces the `.o` files in an LLVM format
5. `emcc`: Compiles the LLVM `.o` files from LLVM in a single js file, `index.js`

Note that because the browser environment has no access to the computer's file system, Emscripten provides a virtual file system that can be accessed using the C++ standard library. JSBSim makes use of `xml` input files to configure the aircraft and the simulator environment. `emcc` provides the `--embed-file` flag which will embed the contents of the given file into the output executable, and makes it available within the virtual file system. Emscripten also provides a way to asynchronously load files from the JS side into the C++ library, but this would require significant changes to the file I/O system in the JSBSim library, which synchronously loads all of its resources.

Compiling the original JSBSim source with no optimizations produces a JS file that's about ~12MB large. This is far too large to efficiently load over the network onto a webpage. Using a `-O2` optimization yields a ~2MB JS file. This is still very large for a JS source, but is similar in size to a large images that may be loaded onto a webpage. Over a normal network connection, the initial load of this file may take 3-5 seconds. It's safe to assume this would be unacceptable for a production application, but is adequate for a prototype. 

Future work on this project may desire to minimize the load time further. This could be done using something like [Webpack](https://webpack.js.org/) or [Browserfy](http://browserify.org/), which will compress all of the assets into a single JS source file.

### Web Application

The web application exists as a front-end for the JSBSim library and manages the user-facing state of the program. The application is written in ClojureScript and uses the Reagent library to manage the components on the screen. Reagent is a ClojureScript wrapper for the popular React.js library. React has the notion of components, which are a single UI element that is driven by some piece of data. Each panel gauge is rendered as a Reagent component. 

#### Skyhawk-Flight-Instruments

There are several open-source libraries that draw flight instruments onto a webpage. One Github project, [uw-ray/Skyhawk-Flight-Instruments](https://github.com/uw-ray/Skyhawk-Flight-Instruments), is a jQuery plugin that draw the basic "six pack" instruments of a Cessna 172: Airspeed indicator, attitude indicator, altimeter, turn coordinator, heading indicator, and vertical speed indicator. The library uses layers of SVGs transformed with CSS to render and animate each instrument. These worked very well out-of-the-box, and were easily wired into JSB simulation as a proof-of-concept. 

![Skyhawk-Flight-Instruments Instruments](img/six-pack.png)


Wiring the jQuery gauges into the ClojureScript/React application proved difficult, however. The jQuery plugin generates and updates the DOM elements in a much different way than idiomatic React. To get around this, the jQuery plugin's initialization code had to be wrapped into a custom React component that correctly dispatched the jQuery update methods. This was using React's `createClass` method to create a component which manually manages its lifecycle.

Here's an example of wrapping the altimeter component:

```{.clj}
; Wrap the jQuery mangament of the component with react
; It's a little ugly, but we gain react's handling of the components
; state
(defn altimeter-component
  [altitude-feet pressure-in-hg]
  (let [jqry-init 
            #(.flightIndicator js/$ "#altimeter" "altimeter" settings)

        ; Have to initialize the atom for the functions to work
        jqry-obj (reagent/atom (jqry-init))]

    (reagent/create-class 
      {:component-did-mount
        #(swap! jqry-obj
          ; Reinitialize the function on component mount and swap!
          (fn [] (jqry-init)))

       :component-will-mount
       #()

       :display-name "altimeter-component"

       :reagent-render
        (fn [altitude-feet pressure-in-hg]
          (. @jqry-obj (setAltitude altitude-feet))
          (. @jqry-obj (setPressure pressure-in-hg false))
          [:div {:id "altimeter" :class "instrument"}])
      })))
```

Notice that we provide custom definitions for `component-did-mount`, `component-will-mount`, and `reagent-render`. The wiring here is not as elegant as the custom-made components (discussed below), but nicely abstracts the jQuery gauges into usable React components. 

#### Gauge Creation

The six-pack instruments worked well as a starting point, but we needed many new instruments to complete the Cessna's panel. The new gauges structured similarly to the Skyhawk instruments. 

Each gauge consists of transparent SVG layers. Consider the heading indicator as a simple example: 

Heading indicator base:

![Heading Indicator Base](img/heading-indicator.svg.png)

Heading indicator ring:

![Heading Indicator Ring](img/heading-indicator-ring.svg.png)

Heading indicator heading bug:

![Heading Indicator Bug](img/heading-indicator-heading-bug.svg.png)


#### Gauge Rendering

If the SVG artwork is well-designed, then rendering and animating the component is very straightforward with ClojureScript/Reagent/React.

Here is an example for the heading indicator above. Note: the actual code here includes a couple more SVG layers than were shown in the previous section.

```{.clj}
(defn heading-indicator [heading-rad heading-bug-rad]
  [:div {:id "heading-indicator" 
         :class "instrument"}
    [:img {:id "bug" 
           :src "img/heading-indicator/heading-indicator-bug.svg"
           :style {
            :transform
                (str "rotate(" (- @heading-bug-rad heading-rad) "rad)")}}]
    [:img {:id "bug-knob" 
           :src "img/heading-indicator/heading-indicator-bug-knob.svg"
           :on-wheel 
              #(do (.preventDefault %)
                (swap! heading-bug-rad + 
                  (cond 
                      (< (scroll-amt %) -2) (deg-to-rad -1)
                      (> (scroll-amt %)  2) (deg-to-rad 1))))
           :on-click 
              #(swap! heading-bug-rad - (deg-to-rad 1))
           :on-context-menu 
            #(do (.preventDefault %)
                 (swap! heading-bug-rad + (deg-to-rad 1)))}]
    [:img {:id "calib-knob"
           :src "img/heading-indicator/heading-indicator-calib-knob.svg"}]
    [:img {:id "ring"
           :src "img/heading-indicator/heading-indicator-ring.svg"
           :style {:transform (str "rotate(" (- heading-rad) "rad)")}}]
    [:img {:id "base"
          :src "img/heading-indicator/heading-indicator-base.svg"}]])
```

As you can see, the entire gauge is wrapped in a `div` element, which each contains a collection of `img` tags that load a single svg image. Events and transformations are configured in the map that is passed to the reagent component. For instance, when the scroll wheel is activated on the "bug-knob" component, then the `heading-bug-rad` atom is incremented by 1 in the direction that the user scrolled. Similar functionality is present in the `on-click` and `on-context-menu` (right-click) events.

In terms of idiomatic React, the `heading-indicator` function represents a component, and the `heading-rad` and `heading-bug-rad` arguments are its props. 

#### Autopilot Tuning

Because it is particularly hard to fly an airplane with only a mouse, this simulator is intended primarily as a procedure trainer. For this reason it's important we include a working autopilot that allows users to command the aircraft without need to make constant stick-and-rudder inputs.

Since it is installed in most newer Cessna's, I implemented a basic version of the [Bendix-King KAP 140](https://bendixking.com/en/products/productitems/kap-140) two-axis autopilot. 

![KAP 140](img/KAP140.gif)

The JSBSim library has an autopilot implementation that we can leverage. It implements basic autopilot functions (attitude hold, heading hold, altitude hold), and is "programmable" via its XML input files. 

For instance, here is component of the altitude-hold mode in the 172's autopilot: 

```{.xml}
  <pid name="fcs/altitude-hold-pid">
    <input> fcs/ap-alt-hold-switch </input>
    <kp> ap/altitude-pid-kp </kp>
    <ki> ap/altitude-pid-ki </ki>
    <kd> ap/altitude-pid-kd </kd>
    <trigger> fcs/windup-trigger </trigger>
    <clipto> <min>-1.0</min>
             <max> 1.0</max> </clipto>
  </pid>
```

This implements a PID loop that outputs to the `fcs/altitude-hold-pid` simulator value. A proportional–integral–derivative controller, or PID loop, is a very common mechanism to drive a control signal that is correlated with some input signal. In this case, the autopilot is controlling the elevator input of the airplane based on an altitude reading. 

Though the autopilot implemented in JSBSim for the Cessna 172 did work out-of-the-box, it did not work very well. It would often blow past its assigned headings or altitudes, and sometimes oscillated around the value. This is usually evidence of a mis-calibrated PID controller. Since this was an important part of the application, I wanted to ensure the autopilot worked reliable before testing the rest of the simulator components. 

To help with this, I used the native JSBSim executable as a command-line program to test different autopilot configurations. Here is a small snippet of Python I used to parameterize the tests: 

```{.py}
import subprocess as sub
import pandas 

sub.run(["./JSBSim",
         "--script=scripts/c172_cruise.xml",
         "--property=ap/altitude-pid-kp=0.05",
         "--property=ap/altitude-pid-ki=0.0035",
         "--property=ap/altitude-pid-kd=0.02"])
df = pandas.read_csv("JSBout172B.csv")
df[0:2000].plot(x="Time", y="Altitude ASL (ft)")
```

Looking at the plots we can get some idea of how the autopilot performs under controlled conditions. This is much more precise than flying the simulator in real-time. Here are a few examples of the aircraft's altitude plotted over time, with the autopilot commanding a climb to a specified altitude.  

Here is what a well-tuned autopilot looks like: 

![AP1](img/ap_altitude_1.png)

Here is one example of a poorly tuned autopilot. This one has the $K_i$ coefficent set too high: 

![AP2](img/ap_altitude_2.png)

As you can see the aircraft is climbing as directed, but the pitch is continuously oscillating.

Here is another badly tuned on, this one has the derivative coefficent, $K_d$ set too high: 

![AP3](img/ap_altitude_3.png)

The values I eventually setting on are by no means perfect. They will still oscillate and overshoot under some conditions, but they work reasonably well for most circumstances. 

### ARINC 424 Parser

One of the biggest challenges with this project is the simulation of the navigation instruments. One of the most basic navigation tools are the very-high-frequency omni-directional range indicators. The sit on the right-most part of the panel to the left of the radios. These instruments respond to a signal from a ground station and displays your angular deviation from a course to or from that station. 

![A VOR Ground Station](img/VOR.JPG)

In the cockpit, the pilot will tune one of the navigation radios (on the right-half of the radio panel) to a specified frequency that is unique to the VOR station. 

![Nav/Comm Radio](img/kx155.jpeg)

The pilot will then enter the desired radial to or from the station into the course deviation indicator (CDI) in the center of the panel. This is done by turning the omni-bearing selector (OBS) knob on the CDI indicator. The CDI will then display the aircraft angular deviation off of the selected VOR radial line. 

![A Course Deviation Indicator](img/cdi.jpg)

In order to simulate a VOR we need a database of the VOR ground stations. We will need the VOR station's frequency and its latitude/longitude position. For flying in the U.S., we can get this information from the FAA in the Coded Instrument Flight Procedures (CIFP) database. The database comes in a textual format which follows the ARINC 424 standard for aircraft navigational data. There is a lot of information published in the CIFP, but for this project we will only need to extract the VOR entries. 

Parsing the data was done in a separate Clojure project. Though I had originally intended to use AWS's DynamoDB for storing and accessing the navigation data, I eventually decided to use a flat-file format instead. DynamoDB is designed for large applications that need highly-consistent read and write access to their data. Since the navigational data only has minor updates once every month there, is no real need to use a managed database service. 

Instead, the standalone Clojure program parses the CIFP database file, parses the entires in it, and produces a JSON file with the relevant VOR information. THe program organizes the VOR data in a series of folders that correspond to 10-degree latitude and longitude sectors. While this requires a little more processing on the client-side than a fully-featured API, the advantage is that we can serve the JSON data files statically just like any other web asset.

The application frontend then makes an AJAX request to load the appropriate JSON data files based on the aircraft's current position. Currently, the application requests the 10-by-10 latitude/longitude degree sector in which the aircraft is currently located, along with the 8 adjacent sectors. The application then parses the JSON files and  caches the VOR data on the client side for later use. 

Once the information is gather on the client-side, the simulation of the instrument is relatively straightforward. We simply locate the nearest VOR station that matches the user-entered frequency, then calculate the bearing to that station. To calculate the bearing, I used the [Geolib](https://github.com/manuelbieh/Geolib) JS library. It provides a `getBearing` function which implements [rhumb line](https://en.wikipedia.org/wiki/Rhumb_line) bearing calculation based on two latitude/longitude points. The CDI needle is then set based on the difference between the bearing to the station, and the pilot-specified omni-bearing selector heading, which is set on the CDI itself. 

## Outcome

Overall I am very pleased with the outcome of this project. While there is certainly more work to be done before this is a fully-fledged training-tool, all of the major features are established. 

As it stands, a user can simply navigate to a webpage and fly the airplane with their keyboard or the autopilot. The user can explore the flight envelope of the aircraft in a variety of flap configurations, and see realistic airspeeds produced by the simulator. The user can also tune the navigation radios and practice finding their position relative to real VOR stations. 

## Future Work

There are numerous features that would add significant realism and training value to the simulator. Here are a few of the major ones: 

- Instrument-landing system (ILS)
- Automatic direction finder (ADF)
- VOR morse code identification
- Outer, inner, and middle markers
- Autopilot NAV and APR hold modes
- Audio panel
- Engine instruments
- Chronometer
- Caution and warning panel
- Menus + Settings

Additionally, the system as a lot of potential as a component in a larger simulator system. It would be relatively straight forward to feed the simulation data from an external simulator like X-Plane, Microsoft Flight Simulator, or Lockheed Martin Prepar3d. This panel could then be used to drive an external display that shows the instrument panel. This would extend the usefulness of the sim considerable, allowing it to be used in more permanent training environment. 